//
//  CategoryViewController.swift
//  Todoey
//
//  Created by Maxim on 1/27/19.
//  Copyright © 2019 oldmencorp. All rights reserved.
//

import UIKit
import CoreData

class CategoryViewController: UITableViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var categoriesArray: [Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadArray()
    }
    
    @IBAction func addTodoeyPressed(_ sender: UIBarButtonItem) {
        var textField: UITextField? = nil
        let alert = UIAlertController(title: "Add new category", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "Add category", style: .default) { (action) in
            let newCategory = Category(context: self.context)
            newCategory.name = textField?.text
            self.categoriesArray.append(newCategory)
            self.tableView.reloadData()
        }
        
        alert.addAction(action)
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Enter category name"
            textField = alertTextField
        }
        
        saveArray()
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: TableView DataSource methods
    
    private func saveArray(){
        do {
            try context.save()
        } catch  {
            print("Error saving context \(error)")
        }
        
        tableView.reloadData()
    }
    
    private func loadArray(with request: NSFetchRequest<Category> = Category.fetchRequest()){
        do{
            self.categoriesArray = try context.fetch(request)
        } catch {
            print("Load form DB error \(error)")
        }
        tableView.reloadData()
    }
    
    //MARK: TableView Delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToTodos", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! TodoListViewController
        if let indexPath = tableView.indexPathForSelectedRow{
            destinationVC.selectedCategory = categoriesArray[indexPath.row]
        }
    }
    
    //MARK: TableView Manipulation methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoriesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        cell.textLabel?.text = categoriesArray[indexPath.row].name
        return cell
    }
}
