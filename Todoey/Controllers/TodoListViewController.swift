//
//  ViewController.swift
//  Todoey
//
//  Created by Maxim on 1/18/19.
//  Copyright © 2019 oldmencorp. All rights reserved.
//

import UIKit
import CoreData

class TodoListViewController: UITableViewController {
    
    var selectedCategory: Category? {
        didSet{
            loadArray()
        }
    }
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var itemArray: [Todo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadArray()
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoItemCell", for: indexPath)
        cell.textLabel?.text = itemArray[indexPath.row].title
        setCheckMark(cell: cell, todo: itemArray[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let todo = itemArray[indexPath.row]
        todo.isChecked = !todo.isChecked
        setCheckMark(cell: tableView.cellForRow(at: indexPath), todo: todo)
        //        context.delete(itemArray[indexPath.row])
        //        itemArray.remove(at: indexPath.row)
        saveArray()
    }
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField: UITextField? = nil
        let alert = UIAlertController(title: "Add todo item", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add item", style: .default) { (action) in
            let newTodo = Todo(context: self.context)
            newTodo.title = textField?.text
            newTodo.parentCategory = self.selectedCategory
            self.itemArray.append(newTodo)
            self.tableView.reloadData()
        }
        
        alert.addAction(action)
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Enter todo title"
            textField = alertTextField
        }
        
        saveArray()
        present(alert, animated: true, completion: nil)
    }
    
    private func setCheckMark(cell: UITableViewCell?, todo: Todo){
        cell?.accessoryType = todo.isChecked ? .checkmark : .none
    }
    
    private func saveArray(){
        do {
            try context.save()
        } catch  {
            print("Error saving context \(error)")
        }
        
        tableView.reloadData()
    }
    
    private func loadArray(predicate: NSPredicate? = nil, sortDescriptor: NSSortDescriptor? = nil){
        let request: NSFetchRequest<Todo> = Todo.fetchRequest()
        let categoryPredicate = NSPredicate(format: "parentCategory.name MATCHES %@", selectedCategory!.name!)
        
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicate != nil ? [categoryPredicate, predicate!] : [categoryPredicate])
        
        do {
            self.itemArray = try context.fetch(request)
        } catch {
            print("Load form DB error \(error)")
        }
        tableView.reloadData()
    }
}

//MARK: - Search bar section

extension TodoListViewController: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text?.isEmpty ?? true {
            loadArray()
        } else {
            loadArray(predicate: NSPredicate(format: "title CONTAINS[cd] %@", searchBar.text!), sortDescriptor: NSSortDescriptor(key: "title", ascending: true))
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            loadArray()
            DispatchQueue.main.async {
                searchBar.resignFirstResponder()
            }
        }
    }
}
